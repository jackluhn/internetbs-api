<?php

namespace JackLuhn\Internetbs\Commands
{
	class AccountPriceList implements CommandInterface
	{

		protected $currency;
		
		protected $version;

		protected $discountCode = '';

		public function __construct($currency = '', $version = 2) 
		{
			$this->currency = $currency;
			$this->version = $version;
		}
		
		public function addDiscountCode($code) {
		
		}
		
		/**
		 * This method returns the api verb (call)
		 * that should be requested/executed by the command
		 */
		public function getPath()
		{
			return 'Account/PriceList/Get';
		}
		
		/**
		 * This method returns the data that should be passed
		 * to the api call
		 */
		public function run()
		{
			$options = array();
			
			$options['DiscountCode'] = $this->discountCode;
					
			if($this->currency != '') {
				$options['Currency'] = $this->currency;
			}
			$options['Version'] = $this->version;
			
			return $options;
		}
		
	}

}

?>

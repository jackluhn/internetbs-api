<?php

namespace JackLuhn\Internetbs
{

	use JackLuhn\Internetbs\Request\RequestInterface;
	use JackLuhn\Internetbs\Commands\CommandInterface;

	class Api
	{
		/**
		 * @brief the request object
		 * 
		 * @var RequestInterface
		 */
		protected $request;
		
		public function __construct(RequestInterface $request)
		{
			$this->request = $request;
		}
		
		public function run(CommandInterface $command)
		{
			return $this->request($command->getPath(), $command->run());	
		}
		
		public function request($path, Array $data)
		{
			return $this->request->make($path, $data);
		}
		
	}
	
}

?>

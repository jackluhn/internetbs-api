<?php

namespace JackLuhn\Internetbs\Commands
{
	class DomainCheck implements CommandInterface
	{

		protected $domain;

		public function __construct($domain) 
		{
			$this->domain = $domain;
		}


		/**
		 * This method returns the api verb (call)
		 * that should be requested/executed by the command
		 */
		public function getPath()
		{
			return 'Domain/Check';
		}
		
		/**
		 * This method returns the data that should be passed
		 * to the api call
		 */
		public function run()
		{
			return array('Domain' => $this->domain);	
		}
		
	}

}

?>
